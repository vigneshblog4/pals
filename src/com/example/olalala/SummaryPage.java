package com.example.olalala;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;




import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SummaryPage extends Activity {
	
	TextView t1,t2,t3,t4;
	Button but;
	private ProgressDialog pDialog;
	EditText ipAddr;
	String succ;
	
	
	JSONParser jParser = new JSONParser();
	int success = 9;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_summary_page);
		
		but=(Button)findViewById(R.id.book);
		t1 =(TextView)findViewById(R.id.sDate);
		t2 =(TextView)findViewById(R.id.eDate);
		t3 =(TextView)findViewById(R.id.sTime);
		t4 =(TextView)findViewById(R.id.eTime);
		ipAddr = (EditText)findViewById(R.id.serverAddress);
		Intent i = getIntent();
		Bundle b = i.getExtras();
		
		String sD = b.getString("sD");
		String eD = b.getString("eD");
		String sT = b.getString("sT");
		String eT = b.getString("eT");
		
		t1.setText(sD);
		t2.setText(eD);
		t3.setText(sT);
		t4.setText(eT);
		but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new Book().execute();
			}
		});
		
	}

	/*
	class book extends AsyncTask<String, String, String>{

		@Override
		
		protected void onPreExecute(){
			super.onPreExecute();
			pDialog= new ProgressDialog(SummaryPage.this);
			pDialog.setMessage("Placing your Order....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
		}
		
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			
			
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			
			String ts1 = t1.getText().toString();
			String ts2 = t2.getText().toString();
			String ts3 = t3.getText().toString();
			String ts4 = t4.getText().toString();
			
			params.add(new BasicNameValuePair("sDate", ts1));
			params.add(new BasicNameValuePair("eDate", ts2));
			params.add(new BasicNameValuePair("sTime", ts3));
			params.add(new BasicNameValuePair("eTime", ts4));
			
			
			
			
			
			
			
			
			//m1.insert(mData);
			
			
			
			//String url = "http://"+ipAddr.getText().toString()+"/ifim/";
			JSONObject json=jsonParser.makeHttpRequest("http://192.168.1.3/ifim/book.php", "POST", params);
                try {
                	
                	
					success = json.getInt("success");
					
					//Log.d("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", String.valueOf(success));
					//userType= json.getString("userType");
					
					
					
					
				} catch (Exception e) {
					// TODO: handle exception
				}
         
			
			return null;
		}
		protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
			if(success==0){
				//Success
				
				Toast.makeText(getApplicationContext(), "Booked", Toast.LENGTH_SHORT).show();
			}
			
			
            pDialog.dismiss();
        }
	}
	*/
	
	class Book extends AsyncTask<String, String, String> {
		 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SummaryPage.this);
            pDialog.setMessage("Booking....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            
            
            String ts1 = t1.getText().toString();
			String ts2 = t2.getText().toString();
			String ts3 = t3.getText().toString();
			String ts4 = t4.getText().toString();
			
			params.add(new BasicNameValuePair("sDate", ts1));
			params.add(new BasicNameValuePair("eDate", ts2));
			params.add(new BasicNameValuePair("sTime", ts3));
			params.add(new BasicNameValuePair("eTime", ts4));
            
            
            //params.add(new BasicNameValuePair("s", "Start"));
			//params.add(new BasicNameValuePair("e", "End"));
            
            String url = "http://192.168.0.11/ifim/book.php";
            JSONObject json = jParser.makeHttpRequest(url, "POST", params);
 
            // Check your log cat for JSON reponse
            
 
            try {
                // Checking for SUCCESS TAG
                success = json.getInt("success");
                //succ = json.getString("aa");
 
               
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
 
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), String.valueOf(success), Toast.LENGTH_SHORT).show();
            // updating UI from Background Thread
            
 
        }
 
    }

}
