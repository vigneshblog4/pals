package com.example.olalala;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class BookingPage extends Activity implements View.OnClickListener{

	
	TextView startDate, endDate, startTime, endTime;;
	DatePickerDialog datePickerDialog;
	RadioGroup rg;
	Button startB,endB, submit;
	Button b1,b2,b3,b4,b5,b6,b7,b8;
	int timeFlag=9;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_booking_page);
		
		
		startDate = (TextView)findViewById(R.id.startDate);
		endDate = (TextView)findViewById(R.id.endDate);
		startTime = (TextView)findViewById(R.id.start_time);
		endTime = (TextView)findViewById(R.id.end_time);
		
		Calendar c = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		
		startDate.setText(formattedDate);
		endDate.setText(formattedDate);
		
		
		startB = (Button)findViewById(R.id.strt_button);
		endB = (Button)findViewById(R.id.end_button);
		submit = (Button)findViewById(R.id.submit);
		
		
		b1 = (Button)findViewById(R.id.ei);
		b1.setOnClickListener(this);
		b2 = (Button)findViewById(R.id.eit);
		b2.setOnClickListener(this);
		b3 = (Button)findViewById(R.id.ni);
		b3.setOnClickListener(this);
		b4 = (Button)findViewById(R.id.nit);
		b4.setOnClickListener(this);
		
		b5 = (Button)findViewById(R.id.te);
		b5.setOnClickListener(this);
		b6 = (Button)findViewById(R.id.tet);
		b6.setOnClickListener(this);
		b7 = (Button)findViewById(R.id.el);
		b7.setOnClickListener(this);
		b8 = (Button)findViewById(R.id.elt);
		b8.setOnClickListener(this);
		
		
		
		startDate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(BookingPage.this,
                        new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								// TODO Auto-generated method stub
								startDate.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);
							}
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
			}
		});
		
		endDate.setOnClickListener(new View.OnClickListener() {
			
			@TargetApi(Build.VERSION_CODES.KITKAT) @SuppressLint("NewApi") @Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(BookingPage.this,
                        new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								// TODO Auto-generated method stub
								endDate.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);
							}
                        }, mYear, mMonth, mDay);
                //datePickerDialog.getDatePicker().setCalendarViewShown(true);
                //datePickerDialog.getDatePicker().setSpinnersShown(false); 
                datePickerDialog.show();
			}
		});
		
		
		startB.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				timeFlag = 1;
			}
		});
		
		endB.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				timeFlag = 2;
			}
		});
		
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String sD = startDate.getText().toString();
				String eD = endDate.getText().toString();
				
				String sT = startTime.getText().toString();
				String eT = endTime.getText().toString();
				
				Bundle b = new Bundle();
				
				b.putString("sD", sD);
				b.putString("eD", eD);
				b.putString("sT", sT);
				b.putString("eT", eT);
				
				Intent i = new Intent(getApplicationContext(), SummaryPage.class);
				i.putExtras(b);
				startActivity(i);
				
			}
		});
		
		  
		
		
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		 switch (v.getId()) {

	        case R.id.ei:
	        	if(timeFlag == 1){
	        		startTime.setText("8:00");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("8:00");
	        	}
	        	else{
	        		
	        	}
	            break;

	        case R.id.eit:
	        	if(timeFlag == 1){
	        		startTime.setText("8:30");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("8:30");
	        	}
	        	else{
	        		
	        	}
	            break;

	        case R.id.ni:
	        	if(timeFlag == 1){
	        		startTime.setText("9:00");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("9:00");
	        	}
	        	else{
	        		
	        	}
	            break;
	        case R.id.nit:
	        	if(timeFlag == 1){
	        		startTime.setText("9:30");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("9:30");
	        	}
	        	else{
	        		
	        	}
	            break;
	            
	        case R.id.te:
	        	if(timeFlag == 1){
	        		startTime.setText("10:00");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("10:00");
	        	}
	        	else{
	        		
	        	}
	            break;

	        case R.id.tet:
	        	if(timeFlag == 1){
	        		startTime.setText("10:30");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("10:30");
	        	}
	        	else{
	        		
	        	}
	            break;

	        case R.id.el:
	        	if(timeFlag == 1){
	        		startTime.setText("11:00");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("11:00");
	        	}
	        	else{
	        		
	        	}
	            break;
	        case R.id.elt:
	        	if(timeFlag == 1){
	        		startTime.setText("11:30");
	        	}
	        	else if(timeFlag == 2){
	        		endTime.setText("11:30");
	        	}
	        	else{
	        		
	        	}
	            break;
	        default:
	            break;
	    }
	}

	
}
