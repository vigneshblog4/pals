package com.example.olalala;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginPage extends Activity {
	
	Button login,signUp;
	TextView createAccount;
	EditText emailId, password;
	String userName;
	int flag=0;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        
        //Intent i = new Intent(getApplicationContext(), BookingPage.class);
        //startActivity(i);
        
        
        login = (Button)findViewById(R.id.loginBtn);
        signUp = (Button)findViewById(R.id.createAccount);
        createAccount = (TextView)findViewById(R.id.createAccount);
        emailId = (EditText)findViewById(R.id.emailid);
        password = (EditText)findViewById(R.id.password);
        
        createAccount.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Create Account", Toast.LENGTH_SHORT).show();	
			}
		});
        
        
        signUp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), SignupPage.class);
				startActivity(i);
			}
		});
        
        
        login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				String e = emailId.getText().toString();
				String p = emailId.getText().toString();
				
				if(e.equals("Pals")){
					//Pals
					userName = "Pals";
					flag=1;
				}
				else if(e.equals("Shashank")){
					userName = "Shashank";
					flag=1;
				}
				else if(e.equals("Vignesh")){
					userName = "Vignesh";
					flag=1;
				}
				else if(e.equals("Shiva")){
					userName = "Shiva";
					flag=1;
				}
				
				
				
				if(flag==1){
					Intent i = new Intent(getApplicationContext(), BookingPage.class);
					Bundle b = new Bundle();
					b.putString("userName", userName);
					i.putExtras(b);
					startActivity(i);
					
				}
				else{
					Toast.makeText(getApplicationContext(), "Email Id or Password is Incorrect !!", Toast.LENGTH_SHORT).show();
				}
				
				
				
				
				
			}
		});
        
    }


    
    
}
